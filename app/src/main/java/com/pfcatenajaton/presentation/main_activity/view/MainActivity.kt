package com.pfcatenajaton.presentation.main_activity.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.pfcatenajaton.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
