package com.pfcatenajaton.presentation.register_activity

interface RegisterContract {

    interface RegisterView {
        fun showProgress()
        fun hideProgress()
        fun showError(errorMessage: String)
        fun navigateToMain()
    }

    interface RegisterPresenter {
        fun attachView(view: RegisterView)
        fun isViewAttached(): Boolean
        fun detachView()
        fun checkEmptyMail(email: String): Boolean
        fun checkValidEmail(email: String): Boolean
        fun checkEmptyPasswords(password: String, passwordAgain: String): Boolean
        fun checkPasswordsMatch(password: String, passwordAgain: String): Boolean
        fun signUp(fullName: String, email: String, password: String)
    }
}