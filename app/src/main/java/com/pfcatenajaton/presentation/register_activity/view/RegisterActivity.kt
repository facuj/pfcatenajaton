package com.pfcatenajaton.presentation.register_activity.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.example.pfcatenajaton.R
import com.example.pfcatenajaton.databinding.ActivityRegisterBinding
import com.pfcatenajaton.base.BaseActivity
import com.pfcatenajaton.domain.interactors.register_interactor.RegisterInteractorImpl
import com.pfcatenajaton.presentation.main_activity.view.MainActivity
import com.pfcatenajaton.presentation.register_activity.RegisterContract
import com.pfcatenajaton.presentation.register_activity.presenter.RegisterPresenter

class RegisterActivity : BaseActivity(), RegisterContract.RegisterView {

    private lateinit var binding: ActivityRegisterBinding
    private lateinit var presenter: RegisterPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)

        presenter = RegisterPresenter(RegisterInteractorImpl())
        presenter.attachView(this)
        binding.apply {
            setContentView(root)
            btnRegister.setOnClickListener {
                signUp()
            }
        }
    }

    private fun signUp() {
        val fullName = binding.etRegisterName.text.toString().trim()
        val email = binding.etRegisterEmail.text.toString().trim()
        val password = binding.etRegisterPassword.text.toString().trim()
        val passwordAgain = binding.etRepeatRegisterPassword.text.toString().trim()

        if (presenter.checkEmptyName(fullName)) {
            binding.etRegisterName.error = getString(R.string.name_empty)
            return
        }

        if (presenter.checkEmptyMail(email)) {
            binding.etRegisterEmail.error = getString(R.string.email_empty)
            return
        }

        if (!presenter.checkValidEmail(email)) {
            binding.etRegisterEmail.error = getString(R.string.email_error)
            return
        }
        if (presenter.checkEmptyPasswords(password, passwordAgain)) {
            binding.etRegisterPassword.error = getString(R.string.empty_field)
            binding.etRepeatRegisterPassword.error = getString(R.string.empty_field)
            return
        }
        if (!presenter.checkPasswordsMatch(password, passwordAgain)) {
            binding.etRegisterPassword.error = getString(R.string.passwords_dont_match)
            binding.etRepeatRegisterPassword.error = getString(R.string.passwords_dont_match)
        }
        presenter.signUp(fullName, email, password)

    }

    override fun showProgress() {
        binding.pbRegister.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        binding.pbRegister.visibility = View.GONE
    }

    override fun showError(errorMessage: String) {
        hideProgress()
        toast(errorMessage)
    }

    override fun navigateToMain() {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }
}