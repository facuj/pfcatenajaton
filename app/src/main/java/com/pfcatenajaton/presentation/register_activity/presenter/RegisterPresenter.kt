package com.pfcatenajaton.presentation.register_activity.presenter

import androidx.core.util.PatternsCompat
import com.pfcatenajaton.domain.interactors.register_interactor.RegisterInteractor
import com.pfcatenajaton.presentation.register_activity.RegisterContract

class RegisterPresenter(registerInteractor: RegisterInteractor) :
    RegisterContract.RegisterPresenter {

    var view: RegisterContract.RegisterView? = null
    var registerInteractor: RegisterInteractor? = null

    init {
        this.registerInteractor = registerInteractor
    }

    override fun attachView(view: RegisterContract.RegisterView) {
        this.view = view
    }

    override fun isViewAttached(): Boolean {
        return view != null
    }

    override fun detachView() {
        view = null
    }

    override fun checkEmptyMail(email: String): Boolean {
        return email.isEmpty()
    }

    override fun checkValidEmail(email: String): Boolean {
        return PatternsCompat.EMAIL_ADDRESS.matcher(email).matches()
    }

    override fun checkEmptyPasswords(password: String, passwordAgain: String): Boolean {
        return password.isEmpty() || passwordAgain.isEmpty()
    }

    override fun checkPasswordsMatch(password: String, passwordAgain: String): Boolean {
        return password == passwordAgain
    }

    fun checkEmptyName(fullName: String): Boolean {
        return fullName.isEmpty()
    }

    override fun signUp(fullName: String, email: String, password: String) {
        view?.showProgress()
        registerInteractor?.signUp(
            fullName,
            email,
            password,
            object : RegisterInteractor.RegisterCallback {
                override fun onRegisterSuccess() {
                    view?.navigateToMain()
                    view?.hideProgress()
                }

                override fun onRegisterFailure(errorMessage: String) {
                    view?.showError(errorMessage)
                    view?.hideProgress()
                }

            }
        )
    }
}