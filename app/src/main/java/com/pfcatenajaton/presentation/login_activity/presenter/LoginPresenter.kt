package com.pfcatenajaton.presentation.login_activity.presenter

import com.pfcatenajaton.domain.interactors.login_interactor.LoginInteractor
import com.pfcatenajaton.presentation.login_activity.LoginContract
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

class LoginPresenter(loginInteractor: LoginInteractor) : LoginContract.Presenter,
    CoroutineScope {

    var view: LoginContract.View? = null
    var loginInteractor: LoginInteractor? = null
    val job = Job()

    init {
        this.loginInteractor = loginInteractor
    }

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    override fun attachView(view: LoginContract.View) {
        this.view = view
    }

    override fun detachView() {
        this.view = null
    }

    override fun detachJob() {
        coroutineContext.cancel()
    }

    override fun isViewAttached(): Boolean {
        return this.view != null
    }

    override fun signInWithEmailAndPassword(email: String, pass: String) {
        launch {
            view?.activateWaitingMode()
            try {
                loginInteractor?.signInWithEmailAndPassword(email, pass)
                if (isViewAttached()) {
                    view?.deactivateWaitingMode()
                    view?.navigateToMain()
                }
            } catch (e: Exception) {
                if (isViewAttached()) {
                    view?.showMessage(e.message)
                    view?.deactivateWaitingMode()
                }
            }
        }
    }

    override fun checkEmptyFields(email: String, pass: String): Boolean {
        return email.isEmpty() || pass.isEmpty()
    }


}