package com.pfcatenajaton.presentation.login_activity.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.example.pfcatenajaton.R
import com.example.pfcatenajaton.databinding.ActivityLoginBinding
import com.pfcatenajaton.base.BaseActivity
import com.pfcatenajaton.domain.interactors.login_interactor.LoginInteractorImpl
import com.pfcatenajaton.presentation.login_activity.LoginContract
import com.pfcatenajaton.presentation.login_activity.presenter.LoginPresenter
import com.pfcatenajaton.presentation.main_activity.view.MainActivity
import com.pfcatenajaton.presentation.register_activity.view.RegisterActivity

class LoginActivity : BaseActivity(), LoginContract.View {

    private lateinit var binding: ActivityLoginBinding
    private lateinit var presenter: LoginPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)

        presenter = LoginPresenter(LoginInteractorImpl())
        presenter.attachView(this)

        binding.apply {
            setContentView(root)
            btnLogin.setOnClickListener {
                login()
            }
            btnLoginNewAccount.setOnClickListener {
                navigateToRegister()
            }
        }

    }

    override fun activateWaitingMode() {
        binding.pbLogin.visibility = View.VISIBLE
    }

    override fun deactivateWaitingMode() {
        binding.pbLogin.visibility = View.GONE
    }

    override fun showMessage(message: String?) {
        message?.let { toast(it) }
    }

    override fun login() {
        val email = binding.etEmail.text.toString().trim()
        val password = binding.etPassword.text.toString().trim()
        if (presenter.checkEmptyFields(email, password)) {
            toast(R.string.complete_both_fields)
        } else {
            presenter.signInWithEmailAndPassword(email, password)
        }
    }

    override fun navigateToRegister() {
        startActivity(Intent(this, RegisterActivity::class.java))
        finish()
    }

    override fun navigateToMain() {
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    override fun onDetachedFromWindow() {
        presenter.detachView()
        presenter.detachJob()
        super.onDetachedFromWindow()
    }

    override fun onDestroy() {
        presenter.detachView()
        presenter.detachJob()
        super.onDestroy()
    }
}