package com.pfcatenajaton.presentation.login_activity

interface LoginContract {
    interface View {
        fun activateWaitingMode()
        fun deactivateWaitingMode()
        fun showMessage(message: String?)
        fun login()
        fun navigateToRegister()
        fun navigateToMain()
    }

    interface Presenter {
        fun attachView(view: LoginContract.View)
        fun detachView()
        fun detachJob()
        fun isViewAttached(): Boolean
        fun checkEmptyFields(email: String, pass: String): Boolean
        fun signInWithEmailAndPassword(email: String, pass: String)
    }
}