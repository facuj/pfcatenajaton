package com.pfcatenajaton.controllers

import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class FBDatabaseController {
    companion object {
        const val USERS = "users"

        fun getUser(userId: String) = FirebaseDatabase.getInstance().getReference(USERS).child(userId)
    }
}