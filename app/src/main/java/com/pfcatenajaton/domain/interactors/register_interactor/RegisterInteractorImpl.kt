package com.pfcatenajaton.domain.interactors.register_interactor

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest

class RegisterInteractorImpl : RegisterInteractor {
    override fun signUp(
        fullName: String,
        email: String,
        password: String,
        registerCallback: RegisterInteractor.RegisterCallback
    ) {
        FirebaseAuth.getInstance()
            .createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val profileUpdates = UserProfileChangeRequest.Builder()
                        .setDisplayName(fullName)
                        .build()
                    FirebaseAuth.getInstance().currentUser?.updateProfile(profileUpdates)
                        ?.addOnCompleteListener {
                            if (it.isSuccessful) {
                                registerCallback.onRegisterSuccess()
                            } else registerCallback.onRegisterFailure(it.exception?.message.toString())
                        }
                } else {
                    registerCallback.onRegisterFailure(
                        task.exception?.message.toString()
                    )
                }
            }
    }
}