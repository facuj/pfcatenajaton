package com.pfcatenajaton.domain.interactors.login_interactor

import com.pfcatenajaton.domain.controllers.FBAuthController
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException

class LoginInteractorImpl : LoginInteractor {
    override suspend fun signInWithEmailAndPassword(
        email: String,
        password: String
    ): Unit = suspendCancellableCoroutine { cancellableContinuation ->
        FBAuthController.getUserWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    cancellableContinuation.resume(Unit)
                } else {
                    cancellableContinuation.resumeWithException(
                        Exception(task.exception?.message)
                    )
                }
            }
    }
}