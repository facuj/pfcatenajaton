package com.pfcatenajaton.domain.interactors.login_interactor

interface LoginInteractor {
    suspend fun signInWithEmailAndPassword(
        email: String,
        password: String
    )
}