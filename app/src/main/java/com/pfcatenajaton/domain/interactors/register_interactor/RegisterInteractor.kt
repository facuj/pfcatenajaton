package com.pfcatenajaton.domain.interactors.register_interactor

interface RegisterInteractor {
    fun signUp(
        fullName: String,
        email: String,
        password: String,
        registerCallback: RegisterCallback
    )

    interface RegisterCallback{
        fun onRegisterSuccess()
        fun onRegisterFailure(errorMessage: String)
    }
}